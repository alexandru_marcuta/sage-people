Sage People UI Testing implementation
=====================

Prerequisites
-------------
To build and run this UI testing project you need the following:

 - Java SE Development Kit 8
 - Apache Maven 3
 
Branches
--------
We are working on one infinite lifetime branch in the phylosophy of the [Gitflow][2].
Specifically:

 - the `master` branch contains the latest development changes

### Features
When developing a new feature, please consider the following:

 - new features are spinned of from `master`.
 - once feature development is completed:
 - changes are reviewed by someone else
 - changes are merged back to `master` as soon as they were validated
 - any bugfix should be performed first on the feature branch, then merged back on
 `master`.

Local Build Instructions
------------------------

To build this project, you need to first pull the source for the following project:
sage-people from: https://bitbucket.org/alexandru_marcuta/sage-people

This would typically be done with the commands 
<pre>
git clone https://alexandru_marcuta@bitbucket.org/alexandru_marcuta/sage-people.git
cd sage-people
git checkout master
</pre>

Maven
----------

### Maven user global settings.xml

If you find you are having issues with Maven resolving dependencies
please check that your settings.xml is empty or does not exist.

### Maven Commands

Some sample maven commands and their actions (not all combinations are shown):

**Installing**

* `mvn clean install`
  * Will build and install this project into local repository. 
  * All the tests will run.
* `mvn clean install -DskipTests`
  * Will build and install this project into local repository.  
  * Tests will **not** run.

**Run Tests**

* `mvn install -P suites.SmokeTests`
  * Will run only the tests which are marked as SmokeTests.
* `mvn install -P suites.AllTests`
  * Will run only the tests which are in the AllTests group.  

**Automation Assignment**

Overview

Sage people is a cloud-based people management product which supports a full employment
journey. One of the aspects of the employment journeys is Performance management, using which
people can create and communicate personal ‘Objectives’ clearly and monitor progress with a range
of effective measures.

**Exercise** - Write automated tests for creating a **New Objective** using
Selenium Webdriver.

The details on how to setup Objectives from the employee portal are documented in the PDF file
attached called “Sage People Objective setting guide”.

In order to access the application that you need to automate, please log into Salesforce with the
login details provided in the email.

* Url:  https://login.salesforce.com/
* Username : trial12746@trial.fairsail.com
* Password : password123

Once logged in, use the **Workforce experience** link to open the Sage People employee portal.

The details on how to setup Objectives from the employee portal are documented in the PDF file
attached called **Sage People Objective setting guide**, under test/resources.