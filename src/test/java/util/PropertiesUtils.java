package util;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

import javax.naming.ConfigurationException;

/**
 * Created by amarcuta on 17/07/2017.
 */
public class PropertiesUtils {

    private static final String PROPERTIES_PATH = "src/test/resources/tests.properties";
    private static Properties properties = new Properties();

    public static void setProperties() throws ConfigurationException, IOException {
        String propertiesPath = null;
        if (propertiesPath == null || propertiesPath.isEmpty()) {
            propertiesPath = PROPERTIES_PATH;
        }

        String filePath = Paths.get(propertiesPath).toAbsolutePath().toString();

        InputStream input = new FileInputStream(filePath);
        properties.load(input);
        input.close();
    }

    public static String getProperty(String name) {
        return properties.getProperty(name);
    }
}
