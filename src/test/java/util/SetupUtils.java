package util;

import java.io.IOException;

import javax.naming.ConfigurationException;

/**
 * Created by amarcuta on 25/09/2017.
 */
public class SetupUtils {

    public static SetupData getSetupData() {
        try {
            PropertiesUtils.setProperties();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String environment = PropertiesUtils.getProperty("environment");
        String webDriverUrl = null;
        String username = null;
        String password = null;
        String firstName = null;
        switch(environment) {
            case "qe" :
                webDriverUrl = PropertiesUtils.getProperty("qe.webdriver.base.url");
                username = PropertiesUtils.getProperty("qe.username");
                password = PropertiesUtils.getProperty("qe.password");
                break;
            case "stage" :
                webDriverUrl = PropertiesUtils.getProperty("stage.webdriver.base.url");
                username = PropertiesUtils.getProperty("stage.username");
                password = PropertiesUtils.getProperty("stage.password");
                firstName = PropertiesUtils.getProperty("stage.name");
                break;
            case "prod" :
                webDriverUrl = PropertiesUtils.getProperty("prod.webdriver.base.url");
                username = PropertiesUtils.getProperty("prod.username");
                password = PropertiesUtils.getProperty("prod.password");
                firstName = PropertiesUtils.getProperty("prod.name");
                break;
            default :
                webDriverUrl = PropertiesUtils.getProperty("qe.webdriver.base.url");
                break;
        }
        return new SetupData(webDriverUrl, username, password, firstName);
    }
}
