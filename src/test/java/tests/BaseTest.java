package tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.TimeUnit;

import pages.WrongPageException;
import util.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by amarcuta on 17/07/2017.
 */
public class BaseTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static String driverPath = "chromedriver/";

    protected static WebDriver driver = null;
    protected static WebDriverWait wait = null;
    protected static SetupData setupData = SetupUtils.getSetupData();

    @BeforeEach
    public void init() {
        logger.info("Launching chrome browser...");
        System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 30);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.navigate().to(setupData.getUrl());
        if (!driver.getTitle().equals("Login | Salesforce")) {
            throw new WrongPageException("Incorrect landing page");
        }
    }

    @AfterEach
    public void tearDown() {
        if (driver != null) {
            driver.manage().deleteAllCookies();
            driver.quit();
        }
    }

    @Test
    public void testInit() {
        String webDriverUrl = setupData.getUrl();
        assertTrue(webDriverUrl.length() > 0);

        if (logger.isInfoEnabled()) {
            logger.info("Webdriver {}", webDriverUrl);
        }
    }
}
