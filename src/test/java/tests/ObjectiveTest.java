package tests;

import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import testdata.Objective;

import stepgroups.LogInStepGroups;
import steps.ObjectiveSteps;
import suites.*;
import util.TestUtils;

import static org.junit.Assert.assertEquals;

public class ObjectiveTest extends BaseTest {

    @ParameterizedTest(name = "{index} ==> {0}")
    @CsvFileSource(resources = "/oneObjective.csv")
    @Category(SmokeTests.class)
    public void testCreateNewObjective(String testName, String objectiveName, String description,
                                       String measure, String strategicGoal, String contributesTo,
                                       String startDate, String endDate, String nextReviewDate,
                                       String weight, String requiredForBonus, String visibility) {
        LogInStepGroups.logIn(setupData.getUsername(), setupData.getPassword(), wait);
        ObjectiveSteps.selectTabContainerNavigation(wait, "Workforce eXperience");
        TestUtils.waitForMilliSeconds(1000);
        ObjectiveSteps.selectServiceBarNavigation(wait, "Performance", "Objectives");
        ObjectiveSteps.createNewObjective(wait);
        Objective objective = new Objective (objectiveName, description, measure,
                strategicGoal, contributesTo, startDate, endDate, nextReviewDate, weight,
                requiredForBonus, visibility);
        ObjectiveSteps.completeObjectiveDetails(wait, objective);
        ObjectiveSteps.submitAction(wait);
        TestUtils.waitForMilliSeconds(500);
        ObjectiveSteps.confirmationAction(wait, "OK");
        TestUtils.waitForMilliSeconds(3000);
        ObjectiveSteps.navigateThroughObjectives(wait, "DRAFT");
        Objective actualObjective = ObjectiveSteps.actionOnObjective(wait, objectiveName,"edit");
        ObjectiveSteps.logOut(wait);

        assertEquals(actualObjective.getObjectiveName(), objectiveName);
        assertEquals(actualObjective.getDescription(), description);
        assertEquals(actualObjective.getMeasure(), measure);
        assertEquals(actualObjective.getStrategicGoal(), strategicGoal);
        assertEquals(actualObjective.getStartDate(), startDate);
        assertEquals(actualObjective.getEndDate(), endDate);
        assertEquals(actualObjective.getNextReviewDate(), nextReviewDate);
        assertEquals(actualObjective.getWeight(), weight);
    }

    @ParameterizedTest(name = "{index} ==> {0}")
    @CsvFileSource(resources = "/objectives.csv")
    @Category(AllTests.class)
    public void testCreateNewObjectiveAssertOnTitle(String testName, String objectiveName, String description,
                                                    String measure, String strategicGoal, String contributesTo,
                                                    String startDate, String endDate, String nextReviewDate,
                                                    String weight, String requiredForBonus, String visibility) {
        LogInStepGroups.logIn(setupData.getUsername(), setupData.getPassword(), wait);
        ObjectiveSteps.selectTabContainerNavigation(wait, "Workforce eXperience");
        TestUtils.waitForMilliSeconds(1000);
        ObjectiveSteps.selectServiceBarNavigation(wait, "Performance", "Objectives");
        ObjectiveSteps.createNewObjective(wait);
        Objective objective = new Objective (objectiveName, description, measure,
                strategicGoal, contributesTo, startDate, endDate, nextReviewDate, weight,
                requiredForBonus, visibility);
        ObjectiveSteps.completeObjectiveDetails(wait, objective);
        ObjectiveSteps.submitAction(wait);
        TestUtils.waitForMilliSeconds(500);
        ObjectiveSteps.confirmationAction(wait, "OK");
        TestUtils.waitForMilliSeconds(3000);
        ObjectiveSteps.navigateThroughObjectives(wait, "DRAFT");
        String actualObjectiveTitle = ObjectiveSteps.getObjectiveTitle(wait);
        ObjectiveSteps.logOut(wait);

        assertEquals(actualObjectiveTitle, objective.getObjectiveName());
    }

    @Test
    public void testRemoveDraftObjectives() {
        LogInStepGroups.logIn(setupData.getUsername(), setupData.getPassword(), wait);
        ObjectiveSteps.selectTabContainerNavigation(wait, "Workforce eXperience");
        TestUtils.waitForMilliSeconds(1000);
        ObjectiveSteps.selectServiceBarNavigation(wait, "Performance", "Objectives");
        ObjectiveSteps.navigateThroughObjectives(wait, "DRAFT");
        TestUtils.waitForMilliSeconds(1000);
        ObjectiveSteps.removeAllObjectives(wait);
        ObjectiveSteps.logOut(wait);
    }
}
