package tests;

import org.junit.jupiter.api.Test;

import stepgroups.LogInStepGroups;

import static org.junit.Assert.assertEquals;

/**
 * Created by amarcuta on 17/07/2017.
 */
public class LogInTest extends BaseTest {

    @Test
    public void testLogIn() {
        String expectedWelcomeMessage  = setupData.getFirstName();
        LogInStepGroups.logIn(setupData.getUsername(), setupData.getPassword(), wait);
        String actualWelcomeMessage = LogInStepGroups.getWelcomeMessage(wait);
        LogInStepGroups.logOut(wait);

        assertEquals(actualWelcomeMessage, expectedWelcomeMessage);
    }
}
