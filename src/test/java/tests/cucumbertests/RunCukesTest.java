package tests.cucumbertests;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Created by Ripon on 11/26/2015.
 */
@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber-htmlreport","json:target/cucumber-report.json"})

public class RunCukesTest {
}