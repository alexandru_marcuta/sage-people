package tests.cucumbertests.steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import cucumber.api.java.en.*;
import pages.WrongPageException;
import util.*;

import static org.junit.Assert.assertTrue;

/**
 * Created by amarcuta on 15/10/2017.
 */
public class BaseUrlStepDefs {
    private WebDriver driver;
    private WebDriverWait wait;
    public static String driverPath = "chromedriver/";
    protected static SetupData setupData = SetupUtils.getSetupData();

    @Given("^I want to insert the Sage URL to a Chrome instance$")
    public void i_want_to_insert_the_Toals_URL_to_a_Chrome_instance() throws Throwable {
        System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 30);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.navigate().to(setupData.getUrl());
        if (!driver.getTitle().equals("Login | Salesforce")) {
            throw new WrongPageException("Incorrect landing page");
        }
    }

    @When("^I enter the URL in the Chrome browser$")
    public void i_enter_the_URL_in_the_Chrome_browser() throws Throwable {
        //send the base URL to the Chrome driver
        driver.navigate().to(setupData.getUrl());
    }

    @Then("^It should open the right page$")
    public void it_should_open_the_right_page() throws Throwable {
        //assertion
        String webDriverUrl = setupData.getUrl();
        assertTrue(webDriverUrl.length() > 0);
        driver.quit();
    }
}
