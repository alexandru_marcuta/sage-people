package suites;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

import tests.*;

/**
 * Created by amarcuta on 27/09/2017.
 */
@RunWith(JUnitPlatform.class)
@SelectClasses( { LogInTest.class  } )
public class AllTests {
}
