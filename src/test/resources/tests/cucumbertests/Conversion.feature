Feature: Check that the base URL is opened correctly

  Formula: Open Chrome driver and insert the Sage URL

  Scenario: Check the Base URL on Chrome

    Given I want to insert the Sage URL to a Chrome instance
    When I enter the URL in the Chrome browser
    Then It should open the right page