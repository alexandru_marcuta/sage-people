package actions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.*;

import testdata.Objective;

import pages.*;
import util.*;

public class NewObjectiveActions {

    private static final Logger LOGGER = LoggerFactory.getLogger(NewObjectiveActions.class);

    public static void clickTabNavigation(WebDriverWait wait, String option) {
        SeleniumUtils.selectListElement(HomePage.navigationContainer(wait), option).click();
    }

    public static void selectDropdownOption(WebDriverWait wait, String option, String subOption) {
        SeleniumUtils.selectLinkFromList(TeamMemberPage.menuItems(wait), option).click();
        SeleniumUtils.selectLinkFromList(TeamMemberPage.menuItems(wait), option).click();
        SeleniumUtils.selectLinkFromList(TeamMemberPage.dropdownOptions(wait, option), subOption).click();
    }

    public static void completeObjectiveFields(WebDriverWait wait, Objective objective) {
        ObjectiveDialog.objectiveField(wait).sendKeys(objective.getObjectiveName());
        ObjectiveDialog.descriptionField(wait).sendKeys(objective.getDescription());
        ObjectiveDialog.measureField(wait).sendKeys(objective.getMeasure());
        SeleniumUtils.selectDropdownElement(ObjectiveDialog.strategicObjectiveDropdown(wait),
                objective.getStrategicGoal()).click();
        ObjectiveDialog.startDateField(wait).clear();
        ObjectiveDialog.startDateField(wait).sendKeys(objective.getStartDate());
        ObjectiveDialog.endDateField(wait).clear();
        ObjectiveDialog.endDateField(wait).sendKeys(objective.getEndDate());
        ObjectiveDialog.nextReviewDateField(wait).clear();
        ObjectiveDialog.nextReviewDateField(wait).sendKeys(objective.getNextReviewDate());
        ObjectiveDialog.measureField(wait).click();
        ObjectiveDialog.dataWeightField(wait).clear();
        ObjectiveDialog.dataWeightField(wait).sendKeys(objective.getWeight());
        ObjectiveDialog.requiredForBonusRadioButtons(objective.getRequiredForBonus(), wait).click();
        ObjectiveDialog.visibilityRadioButtons(objective.getVisibility(), wait).click();
        LOGGER.info(">> All fields of the objective are completed!");
    }

    public static void clickSubmitAction(WebDriverWait wait) {
        ObjectiveDialog.revealButton(wait).click();
    }

    public static void clickAction(WebDriverWait wait, String action) {
        ConfirmationDialog.confirmationButtons(action, wait).click();
        LOGGER.info(">> Objective was revealed!");
    }
}
