package actions;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import testdata.Objective;

import pages.*;
import util.SeleniumUtils;
import util.TestUtils;

import java.util.List;

/**
 * Created by amarcuta on 29/11/2017.
 */
public class TeamMemberActions {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeamMemberActions.class);

    public static void clickLogOut(WebDriverWait wait) {
        TeamMemberPage.selectUserOptionsMenu(wait).click();
        TestUtils.waitForMilliSeconds(200);
        SeleniumUtils.selectLinkFromList(TeamMemberPage.selectUserOptions(wait), "Logout  ").click();
    }

    public static void clickNewObjectiveButton(WebDriverWait wait) {
        TeamMemberPage.plusButton(wait).click();
        LOGGER.info(">> Objective form opened!");
    }

    public static Objective actionOnObjective(WebDriverWait wait, String objectiveTitle, String action) {
        if (TeamMemberPage.objectiveTab(wait).getText().equals("None")) {
            LOGGER.info(">> The objective dashboard is empty.");
        } else {
            List<WebElement> objectiveItems = TeamMemberPage.objectiveItems(wait);
            while  (objectiveItems.size() > 0) {
                if (TeamMemberPage.objectiveTitle(objectiveItems.get(objectiveItems.size() - 1))
                        .getText().equals(objectiveTitle)) {
                    SeleniumUtils.selectOperation(TeamMemberPage.objectiveOpenOperations(objectiveItems
                                    .get(objectiveItems.size() - 1)),
                            action).click();
                    TestUtils.waitForMilliSeconds(2000);
                    Objective objective = new Objective();
                    objective.setObjectiveName(ObjectiveDialog.objectiveField(wait).getAttribute("value"));
                    objective.setDescription(ObjectiveDialog.descriptionField(wait).getAttribute("value"));
                    objective.setMeasure(ObjectiveDialog.measureField(wait).getAttribute("value"));
                    objective.setStrategicGoal(ObjectiveDialog.selectedStrategicObjective(
                            ObjectiveDialog.strategicObjectiveDropdown(wait)).getText());
                    objective.setStartDate(ObjectiveDialog.startDateField(wait).getAttribute("value"));
                    objective.setEndDate(ObjectiveDialog.endDateField(wait).getAttribute("value"));
                    objective.setNextReviewDate(ObjectiveDialog.nextReviewDateField(wait).getAttribute("value"));
                    objective.setWeight(ObjectiveDialog.dataWeightField(wait).getAttribute("value"));
                    ObjectiveDialog.objectiveForm(wait).click();
                    TestUtils.waitForMilliSeconds(1000);
                    LOGGER.info(">> The objective {} was found.", objectiveTitle);
                    return objective;
                } else {
                    LOGGER.info(">> There was no objective found with this name: {}.", objectiveTitle);
                    return null;
                }
            }
            LOGGER.info(">> The objective dashboard was cleared.");
            return null;
        }
        return null;
    }

    public static void deleteAllObjectives(WebDriverWait wait) {
        if (TeamMemberPage.objectiveTab(wait).getText().equals("None")) {
            LOGGER.info(">> The objective dashboard is empty.");
        } else {
            List<WebElement> objectiveItems = TeamMemberPage.objectiveItems(wait);
            while  (objectiveItems.size() > 0) {
                SeleniumUtils.selectOperation(TeamMemberPage.objectiveOperations(objectiveItems.get(0)),
                        "delete").click();
                TestUtils.waitForMilliSeconds(1000);
                ConfirmationDialog.confirmationButtons("OK", wait).click();
                TestUtils.waitForMilliSeconds(2000);
                if (TeamMemberPage.objectiveTab(wait).getText().equals("None")) {
                    break;
                }
                objectiveItems = TeamMemberPage.objectiveItems(wait);
            }
            LOGGER.info(">> The objective dashboard was cleared.");
        }
    }

    public static void clickCurrentObjectives(WebDriverWait wait, String objectiveType) {
        SeleniumUtils.selectListButtonElement(TeamMemberPage.currentObjectiveButtons(wait), objectiveType).click();
        LOGGER.info(">> Draft Objectives button clicked!");
    }

    public static String getObjectiveTitle(WebDriverWait wait) {
        List<WebElement> objectiveItems = TeamMemberPage.objectiveItems(wait);
        return TeamMemberPage.objectiveTitle(objectiveItems.get(0)).getText();
    }
}
