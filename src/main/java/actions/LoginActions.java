package actions;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pages.*;
import util.*;

/**
 * Created by amarcuta on 18/07/2017.
 */
public class LoginActions {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginActions.class);

    public static void setUsername(String username, WebDriverWait wait) {
        LogInPage.usernameField(wait).sendKeys(username);
    }

    public static void setPassword(String password, WebDriverWait wait) {
        LogInPage.passwordField(wait).clear();
        TestUtils.waitForMilliSeconds(200);
        LogInPage.passwordField(wait).sendKeys(password);
    }

    public static void clickSignIn(WebDriverWait wait) {
        LogInPage.signInButton(wait).click();
        LOGGER.info(">> Sign In Button clicked!");
    }

    public static String getWelcomeMessage(WebDriverWait wait) {
        return HomePage.selectProfileMenu(wait).getText();
    }

    public static void clickLogOut(WebDriverWait wait) {
        HomePage.selectProfileMenu(wait).click();
        SeleniumUtils.selectMenuItem(HomePage.selectProfileMenu(wait), "Logout").click();
        LOGGER.info(">> Logout Button clicked!");
    }
}
