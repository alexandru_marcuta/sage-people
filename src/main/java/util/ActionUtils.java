package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

/**
 * Created by amarcuta on 18/07/2017.
 */
public class ActionUtils {

    public static void mouseOverElement(WebDriver webDriver, WebElement webElement){
        Actions actions = new Actions(webDriver);
        Actions moveOverUserTab = actions.moveToElement(webElement);
        moveOverUserTab.build().perform();
        TestUtils.waitForMilliSeconds(1000);
    }
}
