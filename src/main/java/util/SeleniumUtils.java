package util;

import org.openqa.selenium.*;

import java.util.List;

/**
 * Created by amarcuta on 09/10/2017.
 */
public class SeleniumUtils {

    public static WebElement selectListElement(WebElement webElement, String toSelect) {
        for (WebElement option : webElement.findElements(By.tagName("li"))) {
            if (option.getText().equals(toSelect)) {
                return option;
            }
        }
        return null;
    }

    public static WebElement selectListButtonElement(WebElement webElement, String toSelect) {
        for (WebElement option : webElement.findElements(By.tagName("li"))) {
            if (option.findElement(By.tagName("span")).getText().equals(toSelect)) {
                return option;
            }
        }
        return null;
    }

    public static WebElement selectMenuItem(WebElement webElement, String toSelect) {
        WebElement element = webElement.findElement(By.id("userNav-menuItems"));
        for (WebElement option : element.findElements(By.tagName("a"))) {
            if (option.getText().equals(toSelect)) {
                return option;
            }
        }
        return null;
    }

    public static WebElement selectLinkFromList(WebElement webElement, String toSelect) {
        for (WebElement option : webElement.findElements(By.tagName("li"))) {
            WebElement optionToClick = option.findElement(By.tagName("a"));
            if (optionToClick.getText().equals(toSelect)) {
                return optionToClick;
            }
        }
        return null;
    }

    public static WebElement selectDropdownElement(WebElement webElement, String toSelect) {
        for (WebElement option : webElement.findElements(By.tagName("option"))) {
            if (option.getText().equals(toSelect)) {
                return option;
            }
        }
        return null;
    }

    public static WebElement selectButton(List<WebElement> options, String toSelect){
        for (WebElement option : options) {
            if (option.getText().equals(toSelect)) {
                return option;
            }
        }
        return null;
    }

    public static WebElement selectOperation(List<WebElement> operations, String toSelect){
        for (WebElement operation : operations) {
            if (operation.getAttribute("operation-id").equals(toSelect)) {
                return operation.findElement(By.tagName("button"));
            }
        }
        return null;
    }

    public static WebElement selectedElement(List<WebElement> operations){
        for (WebElement element : operations) {
            if (element.getAttribute("selected") != null &&
                    element.getAttribute("selected").equals("true"))  {
                return element;
            }
        }
        return null;
    }
}
