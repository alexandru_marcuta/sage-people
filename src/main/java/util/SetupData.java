package util;

/**
 * Created by amarcuta on 25/09/2017.
 */
public class SetupData {

    private String url;
    private String username;
    private String password;
    private String firstName;

    @Override
    public String toString() {
        return "SetupData{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                '}';
    }

    public SetupData(String url, String username, String password, String firstName) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
