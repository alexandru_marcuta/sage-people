package util;

import java.text.*;
import java.util.*;

/**
 * Created by amarcuta on 17/07/2017.
 */
public class TestUtils {

    public static void waitForMilliSeconds(long milliSeconds){
        try {
            Thread.sleep(milliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitForSeconds(long seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitForMinutes(long minutes){
        try {
            Thread.sleep(minutes * 60 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String randomString(final int length) {
        Random r = new Random(); // perhaps make it a class variable so you don't make a new one every time
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < length; i++) {
            char c = (char)(r.nextInt((int)(Character.MAX_VALUE)));
            sb.append(c);
        }
        return sb.toString();
    }

    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmssSS");
        Date currentDate = new Date();
        String date = dateFormat.format(currentDate).toString();
        return date;
    }

    public static String[] convertStringIntoArrayOfStrings(String str) {
        if (str != null) {
            String string = str.trim();
            String arrayOfStrings[] = string.split(",");
            String[] trimmedArray = new String[arrayOfStrings.length];
            for (int i = 0; i < arrayOfStrings.length; i++) {
                trimmedArray[i] = arrayOfStrings[i].trim();
            }
            return trimmedArray;
        }
        return null;
    }
}
