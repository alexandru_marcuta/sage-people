package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by amarcuta on 17/07/2017.
 */
public class HomePage {

    public static WebElement selectProfileMenu(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("userNav")));
        return element;
    }

    public static WebElement navigationContainer(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("tabContainer")));
        return element.findElement(By.tagName("ul"));
    }
}
