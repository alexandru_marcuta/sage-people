package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.*;

/**
 * Created by amarcuta on 17/07/2017.
 */
public class LogInPage {

    public static WebElement usernameField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("username")));
        return element;
    }

    public static WebElement passwordField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
        return element;
    }

    public static WebElement signInButton(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("Login")));
        return element;
    }
}
