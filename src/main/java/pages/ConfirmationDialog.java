package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

import java.util.List;

import static util.SeleniumUtils.selectButton;

/**
 * Created by amarcuta on 28/11/2017.
 */
public class ConfirmationDialog {

    public static WebElement confirmationButtons(String toSelect, WebDriverWait wait) {
        List<WebElement> options = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
                By.xpath("//div[@class='col-lg-12 confirm-action-btn']/button")));
        return selectButton(options, toSelect);
    }
}
