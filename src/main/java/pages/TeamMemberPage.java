package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by amarcuta on 27/11/2017.
 */
public class TeamMemberPage {

    public static WebElement selectUserOptionsMenu(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//a[@class='user-options__link dropdown-toggle ng-binding']")));
        return element;
    }

    public static WebElement menuItems(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("category_tabs")));
        return element;
    }

    public static WebElement dropdownOptions(WebDriverWait wait, String option) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated
                (By.xpath("//ul[@id='category_tabs']/li/a/span[text()=\"" + option + "\"]" +
                        "/parent::*/following-sibling::ul[@class='dropdown-menu']")));
        return element;
    }

    public static WebElement selectUserOptions(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//a[@class='user-options__link dropdown-toggle ng-binding']/following-sibling::ul[@class='dropdown-menu']")));
        return element;
    }

    public static WebElement plusButton(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//wx-button-open[@operation-id='create']/button")));
        return element;
    }

    public static WebElement currentObjectiveButtons(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//objectives[@process-data='processItem.data']/wx-tabs")));
        return element.findElement(By.tagName("ul"));
    }

    public static List<WebElement> objectiveItems(WebDriverWait wait) {
        WebElement draftTab = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//wx-tab[@label='fHCM2__ProcessState_Draft']")));
        return draftTab.findElements(By.tagName("objectives-item"));
    }

    public static WebElement objectiveTab(WebDriverWait wait) {
        WebElement draftTab = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//wx-tab[@label='fHCM2__ProcessState_Draft']")));
        return draftTab.findElement(By.xpath("//div[@ng-show='$ctrl.tab.selected']"));
    }

    public static List<WebElement> objectiveOperations(WebElement objective) {
        return objective.findElements(By.tagName("wx-button-confirm"));
    }

    public static List<WebElement> objectiveOpenOperations(WebElement objective) {
        return objective.findElements(By.tagName("wx-button-open"));
    }

    public static WebElement objectiveTitle(WebElement objective) {
        return objective.findElement(By.tagName("span"));
    }
}
