package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.*;

import util.SeleniumUtils;

import static util.SeleniumUtils.selectButton;

/**
 * Created by amarcuta on 28/11/2017.
 */
public class ObjectiveDialog {

    public static WebElement objectiveField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//input[@ng-model='data.name']")));
        return element;
    }

    public static WebElement descriptionField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//textarea[@ng-model='data.description']")));
        return element;
    }

    public static WebElement measureField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//textarea[@ng-model='data.measure']")));
        return element;
    }

    public static WebElement strategicObjectiveDropdown(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//select[@ng-model='data.strategicObjective']")));
        return element;
    }

    public static WebElement selectedStrategicObjective(WebElement webElement) {
        WebElement element = SeleniumUtils.selectedElement(webElement.findElements(By.tagName("option")));
        return element;
    }

    public static WebElement contributesToDropdown(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//select[@ng-model='data.contributesTo']")));
        return element;
    }

    public static WebElement startDateField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//datepicker-directive[@ng-model='data.startDate']/div/label/input")));
        return element;
    }

    public static WebElement endDateField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//datepicker-directive[@ng-model='data.endDate']/div/label/input")));
        return element;
    }

    public static WebElement nextReviewDateField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//datepicker-directive[@ng-model='data.nextReviewDate']/div/label/input")));
        return element;
    }

    public static WebElement dataWeightField(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//input[@ng-model='data.weight']")));
        return element;
    }

    public static WebElement requiredForBonusRadioButtons(String toSelect, WebDriverWait wait) {
        List<WebElement> options = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
                By.xpath("//button[@ng-show='data.requiredForBonus==false || data.requiredForBonus==null']")));
        return selectButton(options, toSelect);
    }

    public static WebElement visibilityRadioButtons(String toSelect, WebDriverWait wait) {
        List<WebElement> options = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
                By.xpath("//button[@ng-show='data.isPrivate==false || data.isPrivate==null']")));
        return selectButton(options, toSelect);
    }

    public static WebElement revealButton(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//button[@class='btn modal-save ng-binding ng-isolate-scope']")));
        return element;
    }

    public static WebElement objectiveForm(WebDriverWait wait) {
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//button[@ng-hide='formmodal.$dirty']")));
        return element;
    }
}
