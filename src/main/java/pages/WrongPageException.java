package pages;

/**
 * Created by amarcuta on 09/10/2017.
 */
public class WrongPageException extends RuntimeException {
    public WrongPageException(String message) {
        super(message);
    }
}
