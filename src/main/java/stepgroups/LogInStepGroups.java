package stepgroups;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import steps.LogInSteps;

/**
 * Created by amarcuta on 18/07/2017.
 */
public class LogInStepGroups {

    public static void logIn(String username, String password, WebDriverWait wait) {
        LogInSteps.logIn(username, password, wait);
    }

    public static String getWelcomeMessage(WebDriverWait wait) {
        return LogInSteps.getWelcomeMessage(wait);
    }

    public static void logOut(WebDriverWait wait) {
        LogInSteps.logOut(wait);
    }
}
