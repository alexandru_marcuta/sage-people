package testdata;

/**
 * Created by amarcuta on 01/12/2017.
 */
public class Objective {

    protected String objectiveName;
    protected String description;
    protected String measure;
    protected String strategicGoal;
    protected String contributesTo;
    protected String startDate;
    protected String endDate;
    protected String nextReviewDate;
    protected String weight;
    protected String requiredForBonus;
    protected String visibility;

    public Objective() {
    }

    public Objective(String objectiveName, String description, String measure, String strategicGoal,
                     String contributesTo, String startDate, String endDate, String nextReviewDate,
                     String weight, String requiredForBonus, String visibility) {
        this.objectiveName = objectiveName;
        this.description = description;
        this.measure = measure;
        this.strategicGoal = strategicGoal;
        this.contributesTo = contributesTo;
        this.startDate = startDate;
        this.endDate = endDate;
        this.nextReviewDate = nextReviewDate;
        this.weight = weight;
        this.requiredForBonus = requiredForBonus;
        this.visibility = visibility;
    }

    public String getObjectiveName() {
        return objectiveName;
    }

    public void setObjectiveName(String objectiveName) {
        this.objectiveName = objectiveName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getStrategicGoal() {
        return strategicGoal;
    }

    public void setStrategicGoal(String strategicGoal) {
        this.strategicGoal = strategicGoal;
    }

    public String getContributesTo() {
        return contributesTo;
    }

    public void setContributesTo(String contributesTo) {
        this.contributesTo = contributesTo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getNextReviewDate() {
        return nextReviewDate;
    }

    public void setNextReviewDate(String nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRequiredForBonus() {
        return requiredForBonus;
    }

    public void setRequiredForBonus(String requiredForBonus) {
        this.requiredForBonus = requiredForBonus;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
}
