package steps;

import actions.LoginActions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by amarcuta on 17/07/2017.
 */
public class LogInSteps {

    public static void logIn(String username, String password, WebDriverWait wait) {
        LoginActions.setUsername(username, wait);
        LoginActions.setPassword(password, wait);
        LoginActions.clickSignIn(wait);
    }

    public static String getWelcomeMessage(WebDriverWait wait) {
        return LoginActions.getWelcomeMessage(wait);
    }

    public static void logOut(WebDriverWait wait) {
        LoginActions.clickLogOut(wait);
    }
}
