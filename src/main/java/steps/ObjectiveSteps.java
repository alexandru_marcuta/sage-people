package steps;

import actions.*;

import org.openqa.selenium.support.ui.WebDriverWait;

import testdata.Objective;

public class ObjectiveSteps {

    public static void selectTabContainerNavigation(WebDriverWait wait, String option) {
        NewObjectiveActions.clickTabNavigation(wait, option);
    }

    public static void selectServiceBarNavigation(WebDriverWait wait, String option, String subOption) {
        NewObjectiveActions.selectDropdownOption(wait, option, subOption);
    }

    public static void logOut(WebDriverWait wait) {
        TeamMemberActions.clickLogOut(wait);
    }

    public static void createNewObjective(WebDriverWait wait) {
        TeamMemberActions.clickNewObjectiveButton(wait);
    }

    public static void completeObjectiveDetails(WebDriverWait wait, Objective objective) {
        NewObjectiveActions.completeObjectiveFields(wait, objective);
    }

    public static void submitAction(WebDriverWait wait) {
        NewObjectiveActions.clickSubmitAction(wait);
    }

    public static void navigateThroughObjectives(WebDriverWait wait, String objectiveType) {
        TeamMemberActions.clickCurrentObjectives(wait, objectiveType);
    }

    public static String getObjectiveTitle(WebDriverWait wait) {
         return TeamMemberActions.getObjectiveTitle(wait);
    }

    public static void removeAllObjectives(WebDriverWait wait) {
        TeamMemberActions.deleteAllObjectives(wait);
    }

    public static Objective actionOnObjective(WebDriverWait wait, String objectiveTitle, String action) {
        return TeamMemberActions.actionOnObjective(wait, objectiveTitle, action);
    }

    public static void confirmationAction(WebDriverWait wait, String action) {
        NewObjectiveActions.clickAction(wait, action);
    }
}
